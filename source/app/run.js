function appRun ($state, $rootScope, $detection, $timeout, amMoment) {
    if ($detection.isiOS() || $detection.isWindowsPhone() || $detection.isAndroid()) {
        $rootScope.isMobile = true;
    }

    $rootScope.$on('$stateChangeStart', () => {
        $timeout(() => {
            window.prerenderReady = true;
        }, 10000);
    });

    $rootScope.$on('$stateChangeSuccess', (event, toState) => {
        const navbarTransparentPages = [
            'root.main'
        ];

        $rootScope.isNavbarTransparent = navbarTransparentPages.includes(toState.name);
        $rootScope.isModal = false;
    });

    amMoment.changeLocale('ru');
}

appRun.$inject = ['$state', '$rootScope', '$detection', '$timeout', 'amMoment'];

export default appRun;
