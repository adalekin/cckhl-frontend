function appConfig ($tastypieProvider, $sceDelegateProvider, $mdDateLocaleProvider, moment, uiSelectConfig) {
    let remoteServerAddress = `${location.protocol}//${location.hostname}`;

    /* eslint-disable */
    if (!__PROD__) {
    /* eslint-enable */
        remoteServerAddress = 'http://77.244.214.55';
    }

    $tastypieProvider.setResourceUrl(`${remoteServerAddress}/api/v1/`);

    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        'https://www.youtube.com/**'
    ]);

    $mdDateLocaleProvider.months = ['Январь', 'Февраль', 'Март', 'Апрель',
        'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь',
        'Декабрь'];
    $mdDateLocaleProvider.shortMonths = ['Янв', 'Фев', 'Мар', 'Апр', 'Май',
        'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];
    $mdDateLocaleProvider.days = ['Воскресенье', 'Понедельник', 'Вторник',
        'Среда', 'Четверг', 'Пятница', 'Суббота'];
    $mdDateLocaleProvider.shortDays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт',
        'Сб'];

    // Can change week display to start on Monday.
    $mdDateLocaleProvider.firstDayOfWeek = 1;

    $mdDateLocaleProvider.formatDate = function (date) {
        if (!date) {
            return '';
        }

        const m = moment(date);
        return m.isValid() ? m.format('L') : '';
    };

    uiSelectConfig.theme = 'bootstrap';
    uiSelectConfig.dropdownPosition = 'down';
}

appConfig.$inject = ['$tastypieProvider', '$sceDelegateProvider', '$mdDateLocaleProvider', 'moment', 'uiSelectConfig'];

export default appConfig;
