class BlockCoverMainController {
    constructor ($scope, $timeout, $interval) {
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.carouselBusy = false;

        this.$scope.$watch(() => this.contestSlides, this._slidesChanged());

        $interval(() => {
            if (this.slides) {
                const currentIndex = this._getCurrentSlideIndex();
                if (currentIndex === this.slides.length - 1) {
                    this.goTo(0);
                } else {
                    this.goToNext();
                }
            }
        }, 4000);
    }

    _slidesChanged () {
        return (newVal) => {
            if (newVal) {
                this.slides = newVal;
                this.goTo(0);
            }
        };
    }

    _isGoInProgress () {
        return this.carouselBusy;
    }

    _getCurrentSlideIndex () {
        let result = -1;

        for (let index = 0; index < this.slides.length; index++) {
            if (this.slides[index].current) {
                result = index;
            }
        }

        return result;
    }

    goTo (index) {
        // console.log(`Switching to: ${index}`);
        if (this._isGoInProgress()) {
            return;
        }

        this.carouselBusy = true;

        let currentIndex = this._getCurrentSlideIndex();

        if (currentIndex !== -1) {
            this.slides[currentIndex].current = false;
        }

        if (index < this.slides.length) {
            this.slides[index].current = true;
        }

        if (index === this.slides.length - 1) {
            currentIndex = this.slides.length;
        } else {
            currentIndex = index + 1;
        }

        this.$timeout(() => {
            this.$scope.$apply();
        });

        this.$timeout(() => {
            this.carouselBusy = false;
        }, 1000);
    }

    goToNext () {
        if (this.slides) {
            const currentIndex = this._getCurrentSlideIndex();
            if (currentIndex < this.slides.length - 1) {
                this.goTo(currentIndex + 1);
            }
        }
    }

    goToPrev () {
        if (this.slides) {
            const currentIndex = this._getCurrentSlideIndex();
            if (currentIndex > 0) {
                this.goTo(currentIndex - 1);
            }
        }
    }
}
BlockCoverMainController.$inject = ['$scope', '$timeout', '$interval'];

export default BlockCoverMainController;
