import angular from 'angular';

import BlockCoverMainController from './block.cover.main.controller';
import BlockCoverMainDirective from './block.cover.main.directive';

const blockCoverMain = angular.module('app.components.block.cover.main', [])
    .controller(BlockCoverMainController.name, BlockCoverMainController)
    .directive('cckhlBlockCoverMain', BlockCoverMainDirective);

export default blockCoverMain;
