import BlockCoverMainController from './block.cover.main.controller';
import blockCoverMainHtml from './block.cover.main.jade';

BlockCoverMainDirective.$inject = [];

function BlockCoverMainDirective () {
    return {
        restrict: 'AE',
        scope: {
            contestType: '=',
            contestTitle: '=',
            contestSlides: '=',
            contestButton: '=',
            contestButtonCaption: '=',
            contestButtonRules: '@',
            contestButtonRulesCaption: '=',
            contestCoverImage: '@',
            hideLogo: '='
        },
        controller: BlockCoverMainController.name,
        controllerAs: 'coverMainCtrl',
        bindToController: true,
        template: blockCoverMainHtml
    };
}

BlockCoverMainDirective.$inject = [];

export default BlockCoverMainDirective;
