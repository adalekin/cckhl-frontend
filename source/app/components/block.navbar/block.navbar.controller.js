class BlockNavbarController {
    constructor ($document, $rootScope, $scope, $window) {
        this.$rootScope = $rootScope;
        this.$document = $document;
        this.isOnTop = true;
        angular.element($window).bind('scroll', () => {
            if ($window.pageYOffset > 0) {
                this.isOnTop = false;
            } else {
                this.isOnTop = true;
            }
            $scope.$apply();
        });
    }

    scrollTo (anchor) {
        const element = angular.element(document.getElementById(anchor));
        this.$document.scrollToElement(element, 90, 300);

        this.$rootScope.triggerMenuOpened = false;
    }
}
BlockNavbarController.$inject = ['$document', '$rootScope', '$scope', '$window'];

export default BlockNavbarController;
