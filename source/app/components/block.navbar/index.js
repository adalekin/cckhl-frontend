import angular from 'angular';

import BlockNavbarController from './block.navbar.controller';
import BlockNavbarDirective from './block.navbar.directive';

const blockNavbar = angular.module('app.components.block.navbar', [
])
    .controller(BlockNavbarController.name, BlockNavbarController)
    .directive('cckhlBlockNavbar', BlockNavbarDirective)
    .value('duScrollOffset', 90);

export default blockNavbar;
