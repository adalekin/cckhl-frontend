import BlockNavbarController from './block.navbar.controller';
import blockNavbarHtml from './block.navbar.jade';

BlockNavbarDirective.$inject = [];

function BlockNavbarDirective () {
    return {
        restrict: 'AE',
        scope: {},
        controller: BlockNavbarController.name,
        controllerAs: 'navbarCtrl',
        bindToController: true,
        template: blockNavbarHtml
    };
}

BlockNavbarDirective.$inject = [];

export default BlockNavbarDirective;
