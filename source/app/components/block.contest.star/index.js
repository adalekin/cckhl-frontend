import angular from 'angular';

import BlockContestStarController from './block.contest.star.controller';
import BlockContestStarDirective from './block.contest.star.directive';

const blockContestStar = angular.module('app.components.block.contest.star', [])
    .controller(BlockContestStarController.name, BlockContestStarController)
    .directive('cckhlBlockContestStar', BlockContestStarDirective);

export default blockContestStar;
