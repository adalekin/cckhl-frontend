import BlockContestStarController from './block.contest.star.controller';
import blockContestStarHtml from './block.contest.star.jade';

BlockContestStarDirective.$inject = [];

function BlockContestStarDirective () {
    return {
        restrict: 'AE',
        scope: {},
        controller: BlockContestStarController.name,
        controllerAs: 'contestStarCtrl',
        bindToController: true,
        template: blockContestStarHtml
    };
}

BlockContestStarDirective.$inject = [];

export default BlockContestStarDirective;
