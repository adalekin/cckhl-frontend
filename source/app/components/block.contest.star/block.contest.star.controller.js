class BlockContestStarController {
    constructor ($tastypieResource, $sce, $rootScope) {
        this.$sce = $sce;
        this.$rootScope = $rootScope;
        this.StarVideo = new $tastypieResource('star/video', {limit: 0});

        this.StarVideo.objects.$find().then((data) => {
            data.objects.forEach((obj) => {
                this._preprocessVideo(obj);
            });

            this.starVideoList = data.objects;
        });
    }

    loadMore () {
        this.StarVideo.page.next().then((data) => {
            data.objects.forEach((obj) => {
                this._preprocessVideo(obj);
                this.starVideoList.push(obj);
            });
        });
    }

    _preprocessVideo (starVideo) {
        /* eslint-disable */
        const youtubeVideoId = starVideo.video_link.match(
            /youtube\.com.*(\?v=|\/embed\/)(.{11})/).pop();
        starVideo.image_link = `//img.youtube.com/vi/${youtubeVideoId}/0.jpg`;
        starVideo.video_link = this.$sce.trustAsResourceUrl(
            `//www.youtube.com/embed/${youtubeVideoId}?autoplay=1`
        );
        /* eslint-enable */
    }

    openPopup (starVideo) {
        this.currentStarVideo = starVideo;
        this.$rootScope.isModal = true;
    }

    closePopup () {
        this.currentStarVideo = null;
        this.$rootScope.isModal = false;
    }
}

BlockContestStarController.$inject = ['$tastypieResource', '$sce', '$rootScope'];

export default BlockContestStarController;
