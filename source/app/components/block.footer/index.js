import angular from 'angular';

import BlockFooterController from './block.footer.controller';
import BlockFooterDirective from './block.footer.directive';

const blockFooter = angular.module('app.components.block.footer', [])
    .controller(BlockFooterController.name, BlockFooterController)
    .directive('cckhlBlockFooter', BlockFooterDirective);

export default blockFooter;
