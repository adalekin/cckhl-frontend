import BlockFooterController from './block.footer.controller';
import blockFooterHtml from './block.footer.jade';

BlockFooterDirective.$inject = [];

function BlockFooterDirective () {
    return {
        restrict: 'AE',
        scope: {},
        controller: BlockFooterController.name,
        controllerAs: 'footerCtrl',
        bindToController: true,
        template: blockFooterHtml
    };
}

BlockFooterDirective.$inject = [];

export default BlockFooterDirective;
