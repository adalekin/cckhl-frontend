import BlockContactController from './block.contact.controller';
import blockContactHtml from './block.contact.jade';

BlockContactDirective.$inject = [];

function BlockContactDirective () {
    return {
        restrict: 'AE',
        scope: {},
        controller: BlockContactController.name,
        controllerAs: 'contactCtrl',
        bindToController: true,
        template: blockContactHtml
    };
}

BlockContactDirective.$inject = [];

export default BlockContactDirective;
