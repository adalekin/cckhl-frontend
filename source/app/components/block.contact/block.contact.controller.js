class BlockContactController {
    constructor ($tastypieResource) {
        /* eslint-disable */
        this.AddressEmotion = new $tastypieResource('address/emotion', {limit: 0});
        /* eslint-enable */
        this.AddressEmotion.objects.$find().then((data) => {
            this.emotionAdresses = data.objects;
        });

        /* eslint-disable */
        this.AddressStar = new $tastypieResource('address/star', {limit: 0});
        /* eslint-enable */
        this.AddressStar.objects.$find().then((data) => {
            this.starAddresses = data.objects;
        });
    }
}
BlockContactController.$inject = ['$tastypieResource'];

export default BlockContactController;
