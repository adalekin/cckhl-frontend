import angular from 'angular';

import BlockContactController from './block.contact.controller';
import BlockContactDirective from './block.contact.directive';

const blockContact = angular.module('app.components.block.contact', [])
    .controller(BlockContactController.name, BlockContactController)
    .directive('cckhlBlockContact', BlockContactDirective);

export default blockContact;
