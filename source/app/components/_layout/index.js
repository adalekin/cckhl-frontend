import angular from 'angular';

import LayoutController from './layout.controller';
import appLayoutStates from './layout.route';

import blockNavbar from '../../components/block.navbar';

export default angular.module('app.layout', [
    blockNavbar.name
])
    .config(appLayoutStates)
    .controller(LayoutController.name, LayoutController);
