class LayoutController {
    constructor ($timeout, $rootScope) {
        this.$rootScope = $rootScope;
        $timeout(
            this._done.bind(this), 3500
        );
    }

    _done () {
        this.ready = true;
        this.$rootScope.preloaderGone = this.ready;
    }
}

LayoutController.$inject = ['$timeout', '$rootScope'];

export default LayoutController;
