import layoutHtml from './layout.jade';

appLayoutStates.$inject = ['$locationProvider', '$stateProvider'];

function appLayoutStates ($locationProvider, $stateProvider) {
    $stateProvider
      .state('root', {
          abstract: true,
          url: '',
          controller: 'LayoutController as vm',
          template: layoutHtml,
          sticky: true,
          dsr: true
      });

    $locationProvider.html5Mode(true);
}

export default appLayoutStates;
