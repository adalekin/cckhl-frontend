class BlockContestEmotionController {
    constructor ($tastypieResource, $scope, moment) {
        this.cityList = [];

        this.minDate = new Date(
            2016,
            9,
            17);

        this.maxDate = new Date(
            2016,
            11,
            31);

        this.AddressEmotion = new $tastypieResource('address/emotion', {limit: 0});
        this.AddressEmotion.objects.$find().then((data) => {
            this.addressEmotionList = data.objects;
            this.addressEmotionList.forEach((city) => {
                this.cityList.push({
                    id: city.id,
                    name: city.city
                });
            });
        });

        this.EmotionPhoto = new $tastypieResource('emotion/photo', {limit: 6});
        this.EmotionPhoto.objects.$find().then((data) => {
            this.emotionPhotoList = data.objects;
        });

        $scope.$watch(() => this.filter, () => {
            if (!this.filter) {
                return;
            }

            const filter = {
                limit: 6
            };

            if (this.filter.date) {
                /* eslint-disable */
                filter.created_at__gte = moment(this.filter.date).toISOString();
                filter.created_at__lt = moment(this.filter.date).add(1, 'day').toISOString();
                /* eslint-enable */
            }

            if (this.filter.city) {
                /* eslint-disable */
                filter.game__city = this.filter.city.city;
                /* eslint-enable */
            }

            this.EmotionPhoto = new $tastypieResource('emotion/photo', filter);
            this.EmotionPhoto.objects.$find().then((data) => {
                this.emotionPhotoList = data.objects;
            });
        }, true);
    }

    loadMore () {
        this.isLast();
        this.EmotionPhoto.page.next().then((data) => {
            data.objects.forEach((obj) => {
                this.emotionPhotoList.push(obj);
            });
        });
    }

    isLast () {
        if (this.EmotionPhoto.page.meta) {
            return !this.EmotionPhoto.page.meta.next;
        }

        return false;
    }
}
BlockContestEmotionController.$inject = ['$tastypieResource', '$scope', 'moment'];

export default BlockContestEmotionController;
