import BlockContestEmotionController from './block.contest.emotion.controller';
import blockContestEmotionHtml from './block.contest.emotion.jade';

BlockContestEmotionDirective.$inject = [];

function BlockContestEmotionDirective () {
    return {
        restrict: 'AE',
        scope: {},
        controller: BlockContestEmotionController.name,
        controllerAs: 'contestEmotionCtrl',
        bindToController: true,
        template: blockContestEmotionHtml
    };
}

BlockContestEmotionDirective.$inject = [];

export default BlockContestEmotionDirective;
