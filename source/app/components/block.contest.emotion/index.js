import angular from 'angular';

import BlockContestEmotionController from './block.contest.emotion.controller';
import BlockContestEmotionDirective from './block.contest.emotion.directive';

const blockContestEmotion = angular.module('app.components.block.contest.emotion', [])
    .controller(BlockContestEmotionController.name, BlockContestEmotionController)
    .directive('cckhlBlockContestEmotion', BlockContestEmotionDirective);

export default blockContestEmotion;
