import angular from 'angular';

import BlockVideoController from './block.video.controller';
import BlockVideoDirective from './block.video.directive';

const blockVideo = angular.module('app.components.block.video', [])
    .controller(BlockVideoController.name, BlockVideoController)
    .directive('cckhlBlockVideo', BlockVideoDirective);

export default blockVideo;
