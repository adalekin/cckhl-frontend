import BlockVideoController from './block.video.controller';
import blockVideoHtml from './block.video.jade';

BlockVideoDirective.$inject = [];

function BlockVideoDirective () {
    return {
        restrict: 'AE',
        scope: {
            video: '='
        },
        controller: BlockVideoController.name,
        controllerAs: 'videoCtrl',
        bindToController: true,
        template: blockVideoHtml
    };
}

BlockVideoDirective.$inject = [];

export default BlockVideoDirective;
