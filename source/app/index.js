// angular
import angular from 'angular';
import uiRouter from 'angular-ui-router';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import ngResource from 'angular-resource';
import ngScroll from 'angular-scroll';
import ngAria from 'angular-aria';
import ngMaterial from 'angular-material';
import ngMoment from 'angular-moment';

// components
import layout from './components/_layout';

// routes
import mainRoute from './pages/main/main.route';

// config
import appConfig from './config';
import appRun from './run';

export default angular.module('app', [
    'oc.lazyLoad',
    'adaptive.detection',
    uiRouter,
    ngAnimate,
    ngSanitize,
    ngResource,
    ngScroll,
    ngAria,
    ngMaterial,
    ngMoment,
    'ui.select',
    'ngResourceTastypie',
    layout.name,
    mainRoute.name
])
    .config(appConfig)
    .run(appRun);
