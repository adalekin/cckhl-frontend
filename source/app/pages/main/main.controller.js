class MainController {
    constructor () {
        this.emotionSlides = [
            {
                icon: require('./images/icons/bottle.svg'),
                description: 'Купи Coca-Cola'
            },
            {
                icon: require('./images/icons/selfie.svg'),
                description: 'Сделай эффектное селфи на ледовой арене с Coca-Cola и ' +
                    'размести в Instagram с #ГдеЛедТамКокаКола'
            },
            {
                icon: require('./images/icons/win.svg'),
                description: 'Жди сообщения о победе на стадионе и в Instagram'
            },
            {
                icon: require('./images/icons/cap.svg'),
                description: 'После игры забери кепку с символикой команды и автографом игрока на стойке выдачи призов'
            }
        ];

        this.starSlides = [
            {
                icon: require('./images/icons/bottle.svg'),
                description: 'Купи Coca-Cola'
            },
            {
                icon: require('./images/icons/cola-up.svg'),
                description: 'В перерывах по команде диктора подними стакан Coca-Cola вверх и поддержи хоккеистов!'
            },
            {
                icon: require('./images/icons/screen.svg'),
                description: 'Попади на большой экран и выиграй!'
            },
            {
                icon: require('./images/icons/cap.svg'),
                description: 'После игры забери кепку с символикой команды и автографом игрока на стойке выдачи призов'
            }
        ];
    }
}

MainController.$inject = [];

export default MainController;
