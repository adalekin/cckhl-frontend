import angular from 'angular';

import MainController from './main.controller';

import blockCoverMain from '../../components/block.cover.main';
import blockVideo from '../../components/block.video';
import blockContestEmotion from '../../components/block.contest.emotion';
import blockContestStar from '../../components/block.contest.star';
import blockContact from '../../components/block.contact';
import blockFooter from '../../components/block.footer';

export default angular.module('app.pages.main', [
    blockCoverMain.name,
    blockVideo.name,
    blockContestEmotion.name,
    blockContestStar.name,
    blockContact.name,
    blockFooter.name
])
    .controller(MainController.name, MainController);
