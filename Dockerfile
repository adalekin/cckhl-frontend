FROM nginx

COPY docker/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY dist /usr/share/nginx/html
